import { UnregisterCallback } from "history";
import React from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import { RouteComponentProps } from "react-router-dom";
import request from "request-promise-native";

import { EntryHeadline } from "./data";
import { absUrl, buildQueryString } from "./util";

import Button from "./components/Button";
import Headline from "./components/Headline";
import GRPCTest from "./components/GRPCTest";

interface IEntryListState {
  headlines?: EntryHeadline[];
  searchInput: string;
  unregisterHistoryListener?: UnregisterCallback;
  lastId?: string;
  lastOffset?: number;
  searchQuery: string;
}

class EntryList extends React.Component<RouteComponentProps, IEntryListState> {
  constructor(props: RouteComponentProps) {
    super(props);

    const url = new URL(window.location.toString());
    const searchQuery = url.searchParams.get("q") || "";
    const lastId = url.searchParams.get("lastId") || "";
    this.state = {
      lastId: "",
      searchInput: searchQuery,
      searchQuery,
    };
    this.updateEntries({searchQuery, lastId});
  }

  public componentDidMount() {
    const unregisterHistoryListener = this.props.history.listen((location, action) => {

      // XXX: I"d rather not test against a hardcoded path name
      if (action !== "POP") {
        return;
      }

      if (location.pathname === "/") {
        const newState: any = {};
        if (location.state && ("searchQuery" in location.state)) {
          newState.searchQuery = location.state.searchQuery;
        } else {
          newState.searchQuery = "";
        }
        if (location.state && ("lastId" in location.state)) {
          newState.lastId = location.state.lastId;
        } else {
          newState.lastId = "";
        }
        this.setState(newState);
        this.updateEntries({updateHistory: false, ...newState});
      }
    });

    this.setState({unregisterHistoryListener});
  }

  public componentWillUnmount() {
    const {unregisterHistoryListener} = this.state;
    if (unregisterHistoryListener) {
      unregisterHistoryListener();
    }
  }

  public render() {
    let headlines;
    if (this.state.headlines) {
      headlines = this.state.headlines.map((headline) => {
        return <Headline key={headline.idUrlencoded} headline={headline} />;
      });
      headlines = <div>{headlines}</div>;
    } else {
      headlines = <div><em>Headlines are loading &hellip;</em></div>;
    }

    return (
      <div>
        <Helmet>
        <title>Headlines | Newz</title>
        </Helmet>

        <nav className="navbar navbar-light bg-light mb-3">
          <div className="form-inline">
            <a href="/" className="navbar-brand">Newz Headlines</a>
            <Button className="btn-sm" onClick={this.reload}>Reload</Button>
            &nbsp;
            <Button className="btn-sm" onClick={this.onMoreClicked}>More</Button>
          </div>
          <form className="form-inline" onSubmit={this.onSubmit}>
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              onChange={this.onInputChange}
              value={this.state.searchInput}
            />
          </form>
        </nav>

        {headlines}

      </div>
    );
  }

  private updateEntries = ({searchQuery, lastId, updateHistory = true}:
    {searchQuery?: string, lastId?: string, updateHistory?: boolean}) => {
    const qs: any = {};
    let url = "/api/entries";
    const hist: Map<string, string> = new Map();

    if (searchQuery) {
      url = "/api/search";
      qs.query = searchQuery;
      hist.set("q", searchQuery);
    }

    if (lastId) {
      qs.last_id = lastId;
      hist.set("last_id", lastId);
    }

    if (updateHistory) {
      const queryString = buildQueryString(hist);
      const state = {searchQuery: hist.get("q"), lastId: hist.get("last_id")};
      this.props.history.push(`/?${queryString}`, state);
    }

    const options = {
      method: "GET",
      qs,
      uri: absUrl(url),
    };

    request(options)
      .then((doc) => {
        const headlines = JSON.parse(doc)
          .map((entryObj: any) => new EntryHeadline(entryObj));
        const lastOffset = this.state.lastOffset || 0 + headlines.length;

        this.setState({headlines, lastOffset: lastOffset});
      })
      .catch((err) => {
        console.error(err);
      });
  }

  private reload = () => {
    const {searchQuery, lastId} = this.state;
    this.updateEntries({searchQuery, lastId});
  }

  private onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const searchInput: string = e.target.value;
    this.setState({searchInput});
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const searchQuery = this.state.searchInput.trim();
    this.setState({searchQuery});
    this.updateEntries({searchQuery});
  }

  private onMoreClicked = (e: React.MouseEvent<HTMLButtonElement>) => {
    const { headlines, searchQuery } = this.state;
    if (headlines) {
      const lastId = headlines[headlines.length - 1].idUrlencoded;
      this.setState({ lastId });
      this.updateEntries({lastId, searchQuery: searchQuery || undefined});
    }
  }
}

export default EntryList;
