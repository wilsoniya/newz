import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import EntryPage from "./Entry";
import EntryList from "./EntryList";

const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={EntryList} />
      <Route path="/entry/:entry_id" component={EntryPage} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(<AppRouter />, document.getElementById("app"));
