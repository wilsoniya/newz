import React from "react";

import md5 from "blueimp-md5";

import rangy from "rangy/lib/rangy-core.js";
import "rangy/lib/rangy-classapplier";
import "rangy/lib/rangy-highlighter";
import "rangy/lib/rangy-selectionsaverestore";
import "rangy/lib/rangy-serializer";
import "rangy/lib/rangy-textrange";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import Button from "components/Button";

const SELECTABLE_TEXT_CONTAINER_ID = "XxX-selectable-text-XxX";
const TEMP_HIGHLIGHT_CLASS_NAME = "XxX-temp-XxX";
const HIGHLIGHT_CLASS_NAME = "mark";

/** SelectableText Properties */
export interface ISelectableTextProps {
  /** text, including uninterpreted HTML markup */
  raw_text: string;
  /** mapping from highlight id to serialized selection representation */
  highlightSelections: { [highlightId: string]: string };
  /** callback on when a new selection is made */
  onSelectionCreated: (serialized: string, selectionHtml: string) => void;
  /** callback on when an existing highlight is clicked */
  onHighlightClick: (highlightId: string, selectionHtml: string) => void;
  /** id of active highlight */
  activeHighlightId?: string;
}

/** Displays a Newz SelectableText */
class SelectableText extends React.Component<ISelectableTextProps, {}> {
  public static defaultProps = {
    onHighlightClick: (highlightId: string, selectionHtml: string) => undefined,
    onSelectionCreated: (serialized: string, selectionHtml: string) => undefined,
  };

  private initialized = false;

  constructor(props: ISelectableTextProps) {
    super(props);
  }

  public componentDidMount = () => {
    if (!this.initialized) {
      // @ts-ignore
      rangy.init();
      this.initialized = true;
    }

    this.draw();
  }

  public componentDidUpdate = (prevProps: ISelectableTextProps) => {
    const selectionsEqual = this.getDrawState(this.props) === this.getDrawState(prevProps);

    if (selectionsEqual) {
      // case: highlightSelections haven"t changed; no need to redraw
      return;
    }

    this.draw();
  }

  public render = () => {
    const {raw_text} = this.props;

    return  (
      <div key={this.getDrawState(this.props)}>
        <div
          className="text-justify"
          id={SELECTABLE_TEXT_CONTAINER_ID}
          dangerouslySetInnerHTML={{__html: raw_text}}
        />
      </div>
    );
  }

  private draw = () => {
    if (!this.initialized) {
      return;
    }

    const jqContainer = $(`#${SELECTABLE_TEXT_CONTAINER_ID}`);
    jqContainer.on("mouseup", () => {
      const curSel = window.getSelection().toString();
      if (curSel !== "") {
        this.onSelection();
      }
    });

    const container = $(`#${SELECTABLE_TEXT_CONTAINER_ID}`).get(0);

    for (const highlightId in this.props.highlightSelections) {
      const serialized = this.props.highlightSelections[highlightId];
      // @ts-ignore
      const selection = rangy.deserializeSelection(serialized, container);
      const selectionHtml = selection.toHtml();

      // @ts-ignore
      const applier = rangy.createClassApplier(
        HIGHLIGHT_CLASS_NAME,
        {
          elementAttributes: {
            "data-selection-id": highlightId,
          },
          tagNames: ["span", "a"],
        },
      );

      applier.applyToSelection();
      window.getSelection().empty();

      $(`[data-selection-id="${highlightId}"]`).on("click", () => {
        this.props.onHighlightClick(highlightId, selectionHtml);
      });

      if (highlightId === this.props.activeHighlightId) {
        $(`[data-selection-id="${highlightId}"]`).css("background-color", "red");
      }
    }
  }

  private onSelection = () => {
    // @ts-ignore
    const selection = rangy.getSelection(window);
    const container = $(`#${SELECTABLE_TEXT_CONTAINER_ID}`).get(0);
    // XXX: see if this is ok
    const omitChecksum = true;
    // @ts-ignore
    const serialized = rangy.serializeSelection(selection, omitChecksum, container);

    this.props.onSelectionCreated(serialized, selection.toHtml());
  }

  private getDrawState = (props: ISelectableTextProps): string => {
    const state = Object.keys(props.highlightSelections);
    if (props.activeHighlightId) {
      state.push(props.activeHighlightId);
    }

    const stateId = md5(state.join("|"));

    return stateId;
  }
}

export default SelectableText;
