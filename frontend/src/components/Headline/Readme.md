```jsx

const headline = {
  authors: 'Scoop Johnson',
  dt_published: 'Tue, 18 Dec 2018 16:33:37 GMT ',
  id_urlencoded: 'id',
  title: 'The Rain in Spain',
  link: new URL('http://example.com/the-rain-in-spain'),
};

<Headline headline={headline} />
```
