import React from "react";
import { Link } from "react-router-dom";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import { EntryHeadline } from "data";

interface IHeadlineProps {
  /** Headline content */
  headline: EntryHeadline;
}

/** Displays a Newz Headline */
const Headline = (props: IHeadlineProps) => {
  const {headline} = props;
  const href = "/entry/" + headline.idUrlencoded;

  return (
    <div>
      <div className="media-body">
        <h5 className="mt-2">
          <Link to={href} className="text-dark" dangerouslySetInnerHTML={{__html: headline.title}} />
        </h5>
        <div className="small font-weight-light ml-2 text-muted">
          {headline.dtPublished} /
          <a className="text-muted" target="_blank" href={headline.link.toString()}>
            {headline.link.hostname}
          </a>
        </div>
      </div>
    </div>
  );
};

export default Headline;
