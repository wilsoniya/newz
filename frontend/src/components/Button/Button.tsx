import React from "react";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

interface IButtonProps {
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  children: any;
  className?: string;
}

/** Displays a test message */
const Button = (props: IButtonProps) => {
  const {children, onClick, className} = props;
  const klass = `btn ${className}`;
  return <button className={klass} onClick={onClick}>{children}</button>;
};

Button.defaultProps = {
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => undefined,
};

export default Button;
