The `Button` Component:
```js
<Button className="btn-primary" onClick={() => { alert('hi!'); }}>
  Howdy, World!
</Button>
```
