import React from "react";

interface TestProps {
  /** custom text to display in test */
  message: string;
}

/** Displays a test message */
const Test = (props: TestProps) => {
  const {message} = props;
  return <div>{message}</div>;
}

export default Test;
