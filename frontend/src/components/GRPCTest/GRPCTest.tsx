import React from "react";

import * as grpcWeb from "grpc-web";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import {HelloReply, HelloRequest} from "newz/grpc/helloworld_pb";
import {GreeterPromiseClient} from "newz/grpc/helloworld_grpc_web_pb";

interface IGRPCTestProps {}
interface IGRPCTestState {
  response?: string;
}

export default class GRPCTest extends React.Component<IGRPCTestProps, IGRPCTestState> {
  constructor(props: IGRPCTestProps) {
    super(props);

    this.state = {
      response: undefined,
    };
  }

  public render() {
    const {response} = this.state;
    const rElt = response ? <span>Response: {response}</span> : null;
    return (
      <div>
        <button className="btn btn-primary" onClick={this.onClick}>Click Me!</button>
        {rElt}
      </div>
    );
  }

  private onClick = () => {
    const greeterService = new GreeterPromiseClient(
      "http://localhost:8080", null, null);

    const request = new HelloRequest();
    request.setName("Michael");

    const call = greeterService.sayHello(request, {"custom-header-1": "value1"})
      .then((response: HelloReply) => {
        this.setState({response: response.getMessage()});
      })
      .catch((err: grpcWeb.Error) => {
        console.error(err);
      });
  }
}
