import * as React from "react";

import Button from "components/Button";

export interface IComment {
  comment_body: string;
  commenter_username: string;
  dt_posted: Date;
}

interface ISelectionCommentsProps {
  selectionHtml: string;
  selectionId: string;
  comments: IComment[];
  onSubmit?: (comment: string) => void;
}

interface ISelectionCommentsState {
  commentInProgress: string;
}

class SelectionComments extends React.Component<ISelectionCommentsProps, ISelectionCommentsState> {
  public static defaultProps = {
    onSubmit: (comment: string) => undefined,
  };

  constructor(props: ISelectionCommentsProps) {
    super(props);

    this.state = {
      commentInProgress: "",
    };
  }

  public render = () => {
    const comments = this.props.comments.map((comment, idx) => {
      return (
        <div key={idx}>
          <div className="card mb-1">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-muted">
                {comment.commenter_username} posted on {comment.dt_posted.toString()}
              </h6>
              <p className="card-text">
                {comment.comment_body}
              </p>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div>
        <div className="card text-white bg-secondary mb-1">
          <div
               className="card-body font-italic"
               dangerouslySetInnerHTML={{__html: this.props.selectionHtml }}
          />
        </div>

        <div>
          {comments}
        </div>

        <div>
          <div className="mb-1">
            <textarea
              className="form-control"
              value={this.state.commentInProgress}
              onChange={this.onCommentInProgressChange}
            />
          </div>
          <div>
            <Button className="btn-primary" onClick={this.onSubmit}>Submit</Button>
          </div>
        </div>
      </div>
    );
  }

  private onCommentInProgressChange = (event: any) => {
  // XXX get proper type for param
    this.setState({commentInProgress: event.target.value });
  }

  private onSubmit = () => {
    if (this.props.onSubmit) {
      this.props.onSubmit(this.state.commentInProgress);
    }

    const commentInProgress = "";
    this.setState({commentInProgress});
  }
}

export { ISelectionCommentsProps, SelectionComments };
