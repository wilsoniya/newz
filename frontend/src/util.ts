const absUrl = (path: string): string => {
  return window.location.origin + path;
};

const buildQueryString = (data: Map<string, string>): string => {
  const ret = [];

  for (const [k, v] of data) {
    ret.push(encodeURIComponent(k) + "=" + encodeURIComponent(v));
  }

  return ret.join("&");
};

export { absUrl, buildQueryString };
