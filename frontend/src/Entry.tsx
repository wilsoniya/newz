import React from "react";
import Helmet from "react-helmet";
import request from "request-promise-native";
import uuid4 from "uuid/v4";

import { RouteComponentProps } from "react-router-dom";

import Button from "./components/Button";
import SelectableText from "./components/SelectableText";
import { default as SelectionComments, IComment } from "./components/SelectionComments";

import { IEntry } from "./data";
import { absUrl } from "./util";

import "./Entry.css";

interface IEntryParams {
  entry_id: string;
}

interface IEntryState {
  entry?: IEntry;
  /** mapping from highlight id to serialized selection representation */
  highlightSelections: { [highlightId: string]: string };
  /** currently selected text */
  curSelection?: {serialized: string, selectionHtml: string};
  /** mapping from hightlight ids to lists of corresponding comments  */
  highlightComments: { [highlightId: string]: IComment[] };
  /** id of of active highlight */
  active_highlight?: {highlightId: string, highlightHtml: string};
}

const COMMENTER_USERNAME = "Anonymous";

class EntryPage extends React.Component<RouteComponentProps<IEntryParams>, IEntryState> {
  constructor(props: RouteComponentProps<IEntryParams>) {
    super(props);
    this.state = {
      highlightSelections: {},
      highlightComments: {},
    };

    const pageId = this.props.match.params.entry_id;

    request(absUrl(`/api/entry/${pageId}`))
      .then((str: string) => {
        const entry = JSON.parse(str);
        this.setState({ entry });
      });
  }

  public render() {
    const pageId = this.props.match.params.entry_id;

    let entryElt;
    const {
      entry,
      highlightSelections,
      curSelection,
      highlightComments,
      active_highlight,
    } = this.state;
    const activeHighlightId = active_highlight && active_highlight.highlightId || undefined;
    const comments = activeHighlightId && highlightComments[activeHighlightId] || [];

    const selectionHtml = curSelection ? curSelection.selectionHtml : "";

    let commentBox;
    if (curSelection) {
      commentBox = (
        <div className="sticky-top">
          <SelectionComments
            selectionHtml={selectionHtml}
            selectionId={curSelection.serialized}
            comments={comments}
            onSubmit={this.onCommentSubmit}
          />
        </div>
      );
    } else if (active_highlight) {
      commentBox = (
        <div className="sticky-top">
          <SelectionComments
            selectionHtml={active_highlight.highlightHtml}
            selectionId={active_highlight.highlightId}
            comments={comments}
            onSubmit={this.onCommentSubmit}
          />
        </div>
      );
    } else {
      commentBox = null;
    }

    if (entry) {
      entryElt = (
          <div>
            <div className="row">
              <div className="col">
                <a href={entry.link} target="_blank">
                  <h3 dangerouslySetInnerHTML={{__html: entry.title}} />
                </a>

                <hr />

                <p>
                  <em>
                    <small>
                      Published {entry.dt_published} by {entry.authors}
                    </small>
                  </em>
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <div className="border-right pr-2">
                  <SelectableText
                    raw_text={entry.article_body_html}
                    highlightSelections={highlightSelections}
                    onSelectionCreated={this.onSelectionCreated}
                    onHighlightClick={this.onHighlightClick}
                    activeHighlightId={activeHighlightId}
                  />
                </div>
              </div>
              <div className="col-6">
                {commentBox}
              </div>
            </div>
          </div>
      );
    } else {
      entryElt = (
        <div><em>Loading Entry&hellip;</em></div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>{entry ? entry.title : "Loading..."} | Newz</title>
        </Helmet>

        {entryElt}
      </div>
    );
  }

  private onSelectionCreated = (serialized: string, selectionHtml: string) => {
    const curSelection = {serialized, selectionHtml};
    this.setState({curSelection: curSelection, active_highlight: undefined});
  }

  private onHighlightClick = (highlightId: string, selectionHtml: string) => {
    const activeHighlight = {highlightId, highlightHtml: selectionHtml};
    this.setState({active_highlight: activeHighlight});
  }

  private onCommentSubmit = (commentBody: string) => {
    const {
      curSelection,
      highlightSelections: oldHighlightSelections,
      highlightComments,
      active_highlight,
    } = this.state;

    const highlightId = active_highlight ? active_highlight.highlightId : uuid4();

    let highlightHtml;
    let highlightSelections = oldHighlightSelections;
    if (curSelection) {
     // case: comment being submitted on highlighted text
      const {serialized, selectionHtml} = curSelection;
      highlightHtml = selectionHtml;

      // add new highlighted region, if necessary
      if (!(highlightId in oldHighlightSelections)) {
        const selection = {[highlightId]: serialized};
        highlightSelections = Object.assign({}, oldHighlightSelections, selection);
      }
    } else if (active_highlight) {
      // case: comment being submitted on existing highlight
      highlightHtml = active_highlight.highlightHtml;
    } else {
      // this should not happen
      return;
    }

    const newActiveHighlight = {
      highlightHtml,
      highlightId,
    };

    const comment: IComment = {
      comment_body: commentBody,
      commenter_username: COMMENTER_USERNAME,
      dt_posted: new Date(),
    };

    if (!(highlightId in highlightComments)) {
      // case: no comment list yet for this highlight; create an empty one
      highlightComments[highlightId] = [];
    }

    highlightComments[highlightId].push(comment);

    this.setState({
      active_highlight: newActiveHighlight,
      curSelection: undefined,
      highlightSelections,
      highlightComments,
    });
  }
}

export default EntryPage;
