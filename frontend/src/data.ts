class EntryHeadline {
  public authors: string;
  public dtPublished: string;
  public idUrlencoded: string;
  public title: string;
  public link: URL;

  constructor(obj: any) {
    this.authors = obj.authors;
    this.dtPublished = obj.dt_published;
    this.idUrlencoded = obj.id_urlencoded;
    this.title = obj.title;
    this.link = new URL(obj.link);
  }
}

interface IEntry {
  id: string;
  link: string;
  feed_id: string;
  dt_retrieved: string;
  dt_published: string;
  dt_updated: string;
  raw_feed_entry: string;
  title: string;
  authors: string;
  article_body_html: string;
  article_body_plaintext: string;
}

export { IEntry, EntryHeadline };
