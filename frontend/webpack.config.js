const webpack = require('webpack');
const path = require('path');

module.exports = {
  mode: 'development',
  // put sourcemaps inline
  devtool: 'eval-source-map',

  // entry point of our application, within the `src` directory (which we add to resolve.modules below):
  entry: ['index.tsx'],

  // configure the output directory and publicPath for the devServer
  output: {
    filename: 'app.js',
    publicPath: 'dist',
    path: path.resolve('dist'),
  },

  // configure the dev server to run
  devServer: {
    port: 3000,
    historyApiFallback: {
      disableDotRule: true,
    },
    inline: true,
    proxy: {
      '/api': {
        target: 'http://localhost:8888',
        pathRewrite: {'^/api': ''},
      },
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Headers": "*"
    },
  },

  // tell Webpack to load TypeScript files
  resolve: {
    // Look for modules in .ts(x) files first, then .js
    extensions: ['.ts', '.tsx', '.js'],

    // add 'src' to the modules, so that when you import files you can do so with 'src' as the relative route
    modules: ['src', 'node_modules'],
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        enforce: 'pre',
        use: [
          'tslint-loader',
        ],
      },

      {
        test: /\.tsx?$/,
        use: [
//        'babel-loader',
          'ts-loader'
        ],
        include: path.resolve('src'),
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },

    ],
  },
  node: {
    fs: 'empty', // because webpack/node are trash: https://github.com/request/request/issues/1529

    net: 'empty', // because webpack/node are trash: https://github.com/request/request/issues/1529

    tls: 'empty', // because webpack/node are trash: https://github.com/request/request/issues/1529
  },
   plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
  ],
};

module.exports.resolve.symlinks = false;
