run:
	cd newz-rs && $(MAKE) run & \
	  cd python && venv/bin/run_server & \
	  cd frontend && yarn styleguide & \
	  cd frontend && yarn watch

refresh-feeds:
	cd python && venv/bin/refresh_feeds
