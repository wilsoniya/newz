from setuptools import setup

setup(
    name='newz',
    version='0.1.0',
    long_description=__doc__,
    packages=['newz'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
	'feedparser==5.2.1',
        'html2text==2018.1.9',
        'pysolr==3.8.1',
        'elasticsearch==6.3.1',
        'Flask==1.0.2',
        'grpcio-tools==1.21.1',
    ],
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    entry_points={
        'console_scripts': [
            'tester=newz.tester:main',
            'refresh_feeds=newz.harvester:refresh_feeds',
            'create_tables=newz.data:create_tables',
            'rebuild_search_index=newz.data:rebuild_search_index',
            'run_server=newz.server:run_server',
        ],
    },
)
