#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import json
import logging
import subprocess
import time
import typing

import feedparser # type: ignore
import html2text # type: ignore

from newz import client
from newz import config
from newz import data


LOGGER = logging.getLogger(__name__)


def fetch_content(url: str):
    """Fetches article body content given a url"""
    cmd = [config.READABILITY_BIN_FPATH, url]
    res = subprocess.run(cmd, capture_output=True, check=True)
    LOGGER.info('Successfully fetched content for %s', url)
    return json.loads(res.stdout)


def fetch_from_entry(entry, feed_url: str) -> typing.Optional[data.Entry]:
    """Fetches content from an feed entry structure, formatting results for insertionrefresh_feeds"""
    h2t = html2text.HTML2Text()

    try:
        content = fetch_content(entry.link)
    except subprocess.CalledProcessError as exc:
        content = None
        LOGGER.warning(
                'Readability encountered a problem fetching content',
                exc_info=exc)

    now = datetime.datetime.now()
    entry_json = json.dumps(entry)

    if content is None:
        LOGGER.warning('Readability returned NoneType content for %s',
                entry.link)
        return None

    article_html = content['content']

    return data.Entry(
        id=entry.id,
        link=entry.link,
        feed_id=feed_url, # maybe not the correct value for this
        dt_retrieved=now,
        dt_published=datetime.datetime.fromtimestamp(
            time.mktime(entry.published_parsed)),
        dt_updated=datetime.datetime.fromtimestamp(
            time.mktime(entry.updated_parsed)),
        raw_feed_entry=entry_json,
        title=entry.title,
        authors=entry.author,
        article_body_html=article_html,
        article_body_plaintext=h2t.handle(article_html),
    )


def refresh_feeds():
    """fetches all feeds and fetches down new content"""
    start = datetime.datetime.now()
    index = data.get_search_provider()

    for feed_url in config.FEEDS:
        feed_start = datetime.datetime.now()
        feed = feedparser.parse(feed_url)

        LOGGER.info('Fetched %s entries from feed %s',
                     len(feed.entries), feed_url)

        for entry in feed.entries:
            entry_id = entry.id

            if client.get_entry(entry_id) is not None:
                LOGGER.info('Entry with id %s already stored; Skipping fetch_content()',
                        entry_id)
                continue

            entry_row = fetch_from_entry(entry, feed_url)

            if entry_row is None:
                LOGGER.warning('Nonetype entry for %s; skipping insert',
                entry.id)
                continue

            client.store_entry(entry_row)

#           data.insert_entry(entry_row)
#           index.index_entry(entry_row)

        elapsed = datetime.datetime.now() - feed_start
        LOGGER.info(
            'Completed scan of feed %s in %f seconds', feed_url,
            elapsed.total_seconds())

    elapsed = datetime.datetime.now() - start
    LOGGER.info(
        'Completed refresh_feeds in %f', elapsed.total_seconds())
