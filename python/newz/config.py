FEEDS = [
    'https://theintercept.com/feed/?lang=en',
    'http://jacobinmag.com/feed/',
    'https://www.theguardian.com/us/rss',
    'https://www.truthdig.com/rss-2',
    'https://communemag.com/feed/',
    'http://socialistworker.org/recent/rss.xml',
    'https://aeon.co/feed.rss',
    'http://www.inthesetimes.com/rss',
    'https://thebaffler.com/feed',
]

DB_FILE = '/home/wilsoniya/devel/newz/python/newz.db'

READABILITY_BIN_FPATH = '/home/wilsoniya/devel/newz/readability/index.js'

SOLR_CONN_STRING = 'http://localhost:8983/solr/newz/'

SERVER_LISTEN_PORT = 8888

ELASTICSEARCH_INDEX = 'newz'

FTS_INDEX = True

DEFAULT_NUM_ENTRIES = 25
