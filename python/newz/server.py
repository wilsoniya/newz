#!/usr/bin/env python3


import urllib.parse

import flask # type: ignore

import newz.config
import newz.data

APP = flask.Flask('newz')


@APP.route('/')
def root():
    doc = {
        'name': 'michael wilson',
        'age': 34,
    }
    return flask.json.jsonify(doc)


@APP.route('/entries')
def get_entries():
    """Gets all entries"""
    num = flask.request.args.get('num', newz.config.DEFAULT_NUM_ENTRIES)
    last_id = flask.request.args.get('last_id')
    last_id = urllib.parse.unquote_plus(last_id) if last_id is not None else None
    entries = []

    for entry in newz.data.get_entries_before(_id=last_id, num=num):
        entries.append({
            'id_urlencoded': urllib.parse.quote_plus(entry.id),
            'title': entry.title,
            'dt_published': entry.dt_published,
            'authors': entry.authors,
            'link': entry.link,
        })

    return flask.json.jsonify(entries)


@APP.route('/search')
def search_entries():
    num = flask.request.args.get('num', newz.config.DEFAULT_NUM_ENTRIES)
    query = flask.request.args.get('query')
    last_id = flask.request.args.get('last_id')


    entries = []

    index = newz.data.get_search_provider()

    for entry in index.search(query, last_id, num):
        entries.append({
            'id_urlencoded': urllib.parse.quote_plus(entry.id),
            'title': entry.title,
            'dt_published': entry.dt_published,
            'authors': entry.authors,
            'link': entry.link,
        })

    return flask.json.jsonify(entries)


@APP.route('/entry/<path:entry_id_urlencoded>')
def fetch_entry(entry_id_urlencoded: str):
    entry_id = urllib.parse.unquote_plus(entry_id_urlencoded)
    entry = newz.data.get_entry(entry_id)
    return dict() if entry is None else flask.json.jsonify(entry._asdict())


def run_server():
    APP.run(host='localhost', port=newz.config.SERVER_LISTEN_PORT, debug=True,
            use_reloader=True)
