from __future__ import annotations
import collections
import datetime
import logging
import sqlite3
import threading
import typing

import elasticsearch # type: ignore
import pysolr # type: ignore

from newz import config
from newz.grpc import newz_pb2


DATETIME_FMT = '%Y-%m-%d %H:%M:%S';

LOGGER = logging.getLogger(__name__)

LOCAL = threading.local()


class Entry(typing.NamedTuple):
    id: str
    link: str
    feed_id: str
    dt_retrieved: datetime.datetime
    dt_published: datetime.datetime
    dt_updated: datetime.datetime
    raw_feed_entry: str
    title: str
    authors: str
    article_body_html: str
    article_body_plaintext: str

    def to_grpc(self) -> newz_pb2.Entry:
        return newz_pb2.Entry(
            id=self.id,
            link=self.link,
            feed_id=self.feed_id,
            dt_retrieved=self.dt_retrieved.strftime(DATETIME_FMT),
            dt_published=self.dt_published.strftime(DATETIME_FMT),
            dt_updated=self.dt_updated.strftime(DATETIME_FMT),
            raw_feed_entry=self.raw_feed_entry,
            title=self.title,
            authors=self.authors,
            article_body_html=self.article_body_html,
            article_body_plaintext=self.article_body_plaintext,
        )

    @classmethod
    def from_grpc(cls, grpc_entry: newz_pb2.Entry) -> Entry:
        return cls(
            id=grpc_entry.id,
            link=grpc_entry.link,
            feed_id=grpc_entry.feed_id,
            dt_retrieved=datetime.datetime.strptime(grpc_entry.dt_retrieved, DATETIME_FMT),
            dt_published=datetime.datetime.strptime(grpc_entry.dt_published, DATETIME_FMT),
            dt_updated=datetime.datetime.strptime(grpc_entry.dt_updated, DATETIME_FMT),
            raw_feed_entry=grpc_entry.raw_feed_entry,
            title=grpc_entry.title,
            authors=grpc_entry.authors,
            article_body_html=grpc_entry.article_body_html,
            article_body_plaintext=grpc_entry.article_body_plaintext,
        )


def get_conn_curs(force_new_conn=False):
    if not hasattr(LOCAL, 'conn') or force_new_conn:
        conn = sqlite3.connect(
                config.DB_FILE,
                detect_types=sqlite3.PARSE_DECLTYPES)
        setattr(LOCAL, 'conn', conn)

    conn = LOCAL.conn
    return conn, conn.cursor()


def create_tables():
    """docstring for create_tables"""
    conn, curs = get_conn_curs()
    statements = [
        """CREATE TABLE IF NOT EXISTS entry (
                id varchar(512) NOT NULL PRIMARY KEY,
                link varchar(2048) NOT NULL,
                feed_id varchar(512) NOT NULL,
                dt_retrieved timestamp NOT NULL,
                dt_published timestamp NOT NULL,
                dt_updated timestamp NOT NULL,
                raw_feed_entry text NOT NULL,
                title varchar(512) NOT NULL,
                authors varchar(256) NOT NULL,
                article_body_html text NOT NULL,
                article_body_plaintext text NOT NULL
        )""",
        """CREATE VIRTUAL TABLE IF NOT EXISTS search_index USING fts5 (
                id,
                link,
                title,
                published_dt,
                authors,
                article_body
        )""",
    ]

    for statement in statements:
        conn.execute(statement)

    conn.commit()


def insert_entry(entry: Entry):
    """Inserts an ``Entry`` into the db."""
    query = 'insert into entry values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    conn, curs = get_conn_curs()
    curs.execute(query, tuple(entry))
    conn.commit()

    LOGGER.info('Inserted 1 entry id=%s', entry.id)


def get_entry(entry_id: str) -> typing.Optional[Entry]:
    """Gets an ``Entry`` given an id."""
    query = 'select * from entry where id = ?'
    _, curs = get_conn_curs()
    curs.execute(query, (entry_id,))
    row = curs.fetchone()

    return Entry._make(row) if row is not None else row


def get_all_entries() -> typing.Iterable[Entry]:
    query = 'select * from entry order by dt_published DESC'
    _, curs = get_conn_curs()
    curs.execute(query)

    while True:
        rows = curs.fetchmany()

        if len(rows) == 0:
            return

        for row in rows:
            yield Entry._make(row)


def get_entries_before(_id: typing.Optional[str] = None,
                       num: int = config.DEFAULT_NUM_ENTRIES) -> typing.Iterable[Entry]:
    """Gets entries published before `_id`."""
    if _id is None:
        query = """
            SELECT *
            FROM entry
            ORDER BY dt_published DESC
            LIMIT ?
        """
        params = (num,)
    else:
        query = """
            SELECT *
            FROM entry as e1
            WHERE e1.dt_published < (
                SELECT e2.dt_published
                FROM entry AS e2
                WHERE e2.id = ?
            )
            ORDER BY e1.dt_published DESC
            LIMIT ?
        """
        params = (_id, num)

    _, curs = get_conn_curs()
    curs.execute(query, params)

    while True:
        rows = curs.fetchmany()

        if len(rows) == 0:
            return

        for row in rows:
            yield Entry._make(row)


def get_entries(ids: typing.List[str]) -> typing.Iterable[Entry]:
    """Gets all Entrys having id in ids."""
    qs = ', '.join(['?'] * len(ids))
    query = f'select * from entry where id in ({qs})'
    _, curs = get_conn_curs()
    curs.execute(query, ids)

    while True:
        rows = curs.fetchmany()

        if len(rows) == 0:
            return

        for row in rows:
            yield Entry._make(row)


class SearchIndex:
    """Type which provides a full-text search index"""
    def search(self, query: str, last_id: typing.Optional[str] = None,
               num: int = config.DEFAULT_NUM_ENTRIES) -> typing.Iterable[Entry]:
        """Queries full text search index, returning Entries"""
        raise NotImplementedError()

    def index_entry(self, entry: Entry):
        """Adds **entry** to the search index."""
        raise NotImplementedError()

    def rebuild_search_index(self):
        """Rebuilds search index from scratch."""
        if not config.FTS_INDEX:
            LOGGER.info('Skipping rebuild_search_index because FTS_INDEX = False')
            return

        t0 = datetime.datetime.now()

        self.purge_index()

        num_entries = 0

        for entry in get_all_entries():
            self.index_entry(entry)
            num_entries += 1

        elapsed = datetime.datetime.now() - t0

        LOGGER.info(
                'rebuild_search_index() reindexed %d entries in %f seconds',
                num_entries, elapsed.total_seconds())

    def purge_index(self):
        """Clears all indexed entries."""
        raise NotImplementedError()


def get_search_provider() -> SearchIndex:
    """Constructs and returns a concrete :py:class:`.SearchIndex`."""
    return SqliteFTSIndex()


def rebuild_search_index():
    index = get_search_provider()
    index.rebuild_search_index()


class ElasticSearchIndex(SearchIndex):
    def search(self, query: str, last_id: typing.Optional[str] = None,
               num: int = config.DEFAULT_NUM_ENTRIES) -> typing.Iterable[Entry]:

        if last_id is not None:
            LOGGER.warning('ElasticSearchIndex.search() does not yet support '
                           'the last_id parameter; it was ignored.')
        if num is not None:
            LOGGER.warning('ElasticSearchIndex.search() does not yet support '
                           'the num parameter; it was ignored.')

        es = elasticsearch.Elasticsearch()
        curs = es.search(
            index='newz', doc_type='entry', q=query, stored_fields='id',
            size=50)
        ids = [i['_id'] for i in curs['hits']['hits']]

        return get_entries(ids) if len(ids) > 0 else list()

    def index_entry(self, entry: Entry):
        """Adds **entry** to the search index."""
        if not config.FTS_INDEX:
            LOGGER.info('Skipping info because FTS_INDEX = False')
            return

        doc = dict(
            id=entry.id,
            link_s=entry.link,
            title_t=entry.title,
            published_dt=entry.dt_published.isoformat(),
            authors_t=entry.authors,
            article_body_t=entry.article_body_plaintext,
        )
        solr = pysolr.Solr(config.SOLR_CONN_STRING)
        solr.add([doc], commit=True)
        es = elasticsearch.Elasticsearch()
        es.index(index=config.ELASTICSEARCH_INDEX,
                 doc_type='entry', id=entry.id, body=doc)

        LOGGER.info('Added entry to search index id=%s', entry.id)

    def purge_index(self):
        solr = pysolr.Solr(config.SOLR_CONN_STRING)
        solr.delete(q='*:*', commit=True)
        es = elasticsearch.Elasticsearch()
        try:
            es.indices.delete(index=config.ELASTICSEARCH_INDEX)
        except elasticsearch.exceptions.NotFoundError:
            LOGGER.warning(
                'elasticsearch index cannot be deleted because it doesn\'t exist: %s',
                config.ELASTICSEARCH_INDEX)


class SqliteFTSIndex(SearchIndex):
    """Type which provides a full-text search index"""
    def search(self, query: str, last_id: typing.Optional[str] = None,
               num: int = config.DEFAULT_NUM_ENTRIES) -> typing.Iterable[Entry]:
        if last_id is not None:
            sql = """
                SELECT id
                FROM search_index
                WHERE search_index MATCH ?
                where published_dt < (
                    SELECT dt_published
                    FROM entry
                    WHERE id = ?
                )
                ORDER BY published_dt DESC
                LIMIT ?
                """
            params = (query, last_id, num)
        else:
            sql = """
                SELECT id
                FROM search_index
                WHERE search_index MATCH ?
                ORDER BY published_dt DESC
                LIMIT ?
                """
            params = (query, num) # type: ignore

        _, curs = get_conn_curs()
        curs.execute(sql, params)
        rows = curs.fetchall()
        ids = [_id for (_id,) in rows]
        return get_entries(ids) if len(ids) > 0 else list()

    def index_entry(self, entry: Entry):
        params = (
            entry.id,
            entry.link,
            entry.title,
            entry.dt_published,
            entry.authors,
            entry.article_body_plaintext,
        )

        query = """INSERT INTO search_index (
                    id,
                    link,
                    title,
                    published_dt,
                    authors,
                    article_body
                ) VALUES (?, ?, ?, ?, ?, ?)"""
        conn, curs = get_conn_curs()
        curs.execute(query, params)

        LOGGER.info('Indexed 1 entry id=%s', entry.id)

    def purge_index(self):
        query = 'DELETE FROM search_index'
        conn, curs = get_conn_curs()
        curs.execute(query)
        conn.commit()

        LOGGER.info('SqliteFTSIndex purged search index.')
