import typing

import grpc

from newz.data import Entry
from newz.grpc.newz_pb2 import (
    AddEntryRequest,
    GetEntryRequest,
    MaybeAddEntryResponse,
)
from newz.grpc.newz_pb2_grpc import EntriesStub

def store_entry(entry: Entry) -> None:
    grpc_entry = entry.to_grpc()

    with grpc.insecure_channel('localhost:50052') as channel:
        stub = EntriesStub(channel)
        request = AddEntryRequest(entry=grpc_entry)
        response = stub.AddEntry(request)


def get_entry(entry_id: str) -> typing.Optional[Entry]:
    with grpc.insecure_channel('localhost:50052') as channel:
        stub = EntriesStub(channel)
        request = GetEntryRequest(id=entry_id)
        response = stub.GetEntry(request)

        return Entry.from_grpc(response.response.entry) \
                if response.HasField('response') \
                and response.response.HasField('entry') \
                else None
