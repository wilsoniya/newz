#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase
from datetime import datetime

from newz import config
from newz import data
from newz.grpc import newz_pb2

ENTRY = data.Entry(
    id='id',
    link='link',
    feed_id='feed_id',
    dt_retrieved=datetime.now(),
    dt_published=datetime.now(),
    dt_updated=datetime.now(),
    raw_feed_entry='raw_feed_entry',
    title='title',
    authors='authors',
    article_body_html='article_body_html',
    article_body_plaintext='article_body_plaintext',
)


class TestEntryToGrpc(TestCase):
    def test_basic(self):
        expected =  newz_pb2.Entry(
            id=ENTRY.id,
            link=ENTRY.link,
            feed_id=ENTRY.feed_id,
            dt_retrieved=ENTRY.dt_retrieved.strftime(data.DATETIME_FMT),
            dt_published=ENTRY.dt_published.strftime(data.DATETIME_FMT),
            dt_updated=ENTRY.dt_updated.strftime(data.DATETIME_FMT),
            raw_feed_entry=ENTRY.raw_feed_entry,
            title=ENTRY.title,
            authors=ENTRY.authors,
            article_body_html=ENTRY.article_body_html,
            article_body_plaintext=ENTRY.article_body_plaintext,
        )
        actual = ENTRY.to_grpc()

        assert expected == actual


class DataTestCase(TestCase):
    def setUp(self):
        super().setUp()
        config.DB_FILE = ':memory:'
        _, _ = data.get_conn_curs(force_new_conn=True)
        data.create_tables()


class TestHousekeeping(TestCase):
    def test_create_tables(self):
        config.DB_FILE = ':memory:'
        data.create_tables()

    def test_insert_1(self):
        config.DB_FILE = ':memory:'
        data.create_tables()

        data.insert_entry(ENTRY)

        conn, curs = data.get_conn_curs()

        curs.execute('select * from entry')
        actual = curs.fetchall()
        self.assertEqual(1, len(actual))


class TestGetEntry(DataTestCase):
    """Test Cases for data.get_entry()"""
    def test_basic(self):
        """get_entry() basic functioning"""
        data.insert_entry(ENTRY)
        actual = data.get_entry('id')
        self.assertEqual('id', actual[0])

    def test_bogus_id(self):
        """get_entry() returns None when given a nonexistent id"""
        actual = data.get_entry('bogus_id')
        expected = None
        self.assertEqual(expected, actual)


class TestGetAllEntries(DataTestCase):
    """Test Cases for data.get_entry()"""
    def test_basic(self):
        """get_entry() basic functioning"""
        data.insert_entry(ENTRY)
        actual = list(data.get_all_entries())
        self.assertEqual(1, len(actual))
        self.assertEqual('id', actual[0].id)

    def test_multiple(self):
        """get_entry() with multiple entries"""
        entry_copy = ENTRY._replace(id='id2')
        data.insert_entry(ENTRY)
        data.insert_entry(entry_copy)
        actual = list(data.get_all_entries())
        self.assertEqual(2, len(actual))
        self.assertEqual('id', actual[0].id)
        self.assertEqual('id2', actual[1].id)


class TesteGetEntriesBefore(DataTestCase):
    """Test Cases for data.get_entries_before()"""
    def test_basic(self):
        entry2 = ENTRY._replace(id='id2', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 1))
        entry3 = ENTRY._replace(id='id3', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 2))
        entry4 = ENTRY._replace(id='id4', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 3))
        data.insert_entry(ENTRY)
        data.insert_entry(entry2)
        data.insert_entry(entry3)
        data.insert_entry(entry4)

        actual = [e.id for e in data.get_entries_before('id4', num=2)]
        expected = ['id3', 'id2',]
        assert expected == actual

    def test_default_params(self):
        entry2 = ENTRY._replace(id='id2', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 1))
        entry3 = ENTRY._replace(id='id3', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 2))
        entry4 = ENTRY._replace(id='id4', dt_published=ENTRY.dt_published.replace(year=ENTRY.dt_published.year + 3))
        data.insert_entry(ENTRY)
        data.insert_entry(entry2)
        data.insert_entry(entry3)
        data.insert_entry(entry4)

        actual = [e.id for e in data.get_entries_before()]
        expected = ['id4', 'id3', 'id2', 'id']
        assert expected == actual


class TestGetEntries(DataTestCase):
    """Test cases for data.get_entries()"""
    def test_basic(self):
        """basic functioning for get_entries()"""
        entry2 = ENTRY._replace(id='id2')
        entry3 = ENTRY._replace(id='id3')
        data.insert_entry(ENTRY)
        data.insert_entry(entry2)
        data.insert_entry(entry3)

        actual = list(data.get_entries(['id2', 'id3']))

        self.assertEqual(set(actual), set([entry2, entry3]))
