#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from newz import harvester

class TestFetchContent(TestCase):
    """Test cases for harvester.fetch_content()"""
    def test_test(self):
        url = 'https://www.jacobinmag.com/2018/02/workweek-free-time-precarity-daylight-savings-time'
        content = harvester.fetch_content(url)
        self.assertEqual('Time After Capitalism', content['title'])
