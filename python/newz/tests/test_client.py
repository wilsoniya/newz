import datetime
import uuid

from newz import client
from newz.data import Entry

def test_store_entry_smoke():
    """newz.client.store_entry() smoke test"""
    entry = Entry(
        id=str(uuid.uuid4()),
        link="bar",
        feed_id="feed_id",
        dt_retrieved=datetime.datetime(2000, 1, 1),
        dt_published=datetime.datetime(2000, 1, 1),
        dt_updated=datetime.datetime(2000, 1, 1),
        raw_feed_entry="raw_feed_entry",
        title="title",
        authors="authors",
        article_body_html="article_body_html",
        article_body_plaintext="article_body_plaintext",
    )

    client.store_entry(entry)


def test_get_entry():
    """newz.client.get_entry() smoke test"""
    entry_id = '86a7cd31-5d1e-4c06-9164-b1edece4509d'
    result = client.get_entry(entry_id)
