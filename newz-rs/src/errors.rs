use chrono;
use diesel;

pub enum NewzError {
    DbError(diesel::result::Error),
    NoResults,
    ConversionError(String),
}

impl From<diesel::result::Error> for NewzError {
    fn from(error: diesel::result::Error) -> Self {
        NewzError::DbError(error)
    }
}

impl From<chrono::format::ParseError> for NewzError {
    fn from(error: chrono::format::ParseError) -> Self {
        NewzError::ConversionError(error.to_string())
    }
}

impl std::fmt::Display for NewzError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            NewzError::ConversionError(error) => {
                write!(f, "Conversion Error: {}", error)
            }
            NewzError::DbError(error) => {
                write!(f, "DB Error: {}", error)
            },
            NewzError::NoResults => {
                write!(f, "No Results")
            }
        }
    }
}
