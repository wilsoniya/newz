#[macro_use]
extern crate diesel;

#[macro_use]
extern crate log;

extern crate pretty_env_logger;

mod data;
mod errors;
mod models;
mod newz_grpc;
mod schema;
mod service;

use std::thread;

use newz_grpc::helloworld_grpc::{Greeter, GreeterServer};
use newz_grpc::helloworld::{HelloRequest, HelloReply};

use service::get_server;


fn main() {
    pretty_env_logger::init();

    let port = 50052;

    let mut server = grpc::ServerBuilder::new_plain();
    server.http.set_port(port);
    server.add_service(get_server());
    server.http.set_cpu_pool_threads(4);

    let _server = server.build().expect("server");

    info!("newzd server started on port {} without TLS", port);

    loop {
        thread::park();
    }
}
