table! {
    entry (id) {
        id -> Text,
        link -> Text,
        feed_id -> Text,
        dt_retrieved -> Timestamp,
        dt_published -> Timestamp,
        dt_updated -> Timestamp,
        raw_feed_entry -> Text,
        title -> Text,
        authors -> Text,
        article_body_html -> Text,
        article_body_plaintext -> Text,
    }
}
