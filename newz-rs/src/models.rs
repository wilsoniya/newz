use core::convert::TryFrom;

use chrono::NaiveDateTime;
use diesel::prelude::*;

use crate::errors::NewzError;
use crate::newz_grpc::newz::{
    Entry as GRPCEntry,
    Error,
    GetEntryResponse,
    MaybeGetEntryResponse,
};
use crate::schema::entry::dsl::*;
use crate::schema::entry;

static DATETIME_FMT: &str = "%Y-%m-%d %H:%M:%S";

#[derive(Clone, Insertable, Queryable)]
#[table_name="entry"]
pub struct Entry {
    pub id: String,
    pub link: String,
    pub feed_id: String,
    pub dt_retrieved: NaiveDateTime,
    pub dt_published: NaiveDateTime,
    pub dt_updated: NaiveDateTime,
    pub raw_feed_entry: String,
    pub title: String,
    pub authors: String,
    pub article_body_html: String,
    pub article_body_plaintext: String,
}

impl Entry {
    pub fn get(conn: &PgConnection, _id: String) -> Result<Self, NewzError> {
        entry.filter(id.eq(_id))
            .limit(1)
            .load::<Self>(conn)
            .map_err(|err| err.into())
            .and_then(|results| {
                results.first()
                    .map(|entry_ref| entry_ref.to_owned())
                    .ok_or(NewzError::NoResults)
            })
    }

    pub fn after_published(
        conn: &PgConnection,
        after: NaiveDateTime,
        limit: u8
    ) -> Result<Vec<Self>, NewzError> {
        entry.filter(dt_published.gt(after))
            .limit(limit as i64)
            .load::<Self>(conn)
            .map_err(|err| err.into())
    }

    pub fn insert(conn: &PgConnection, _entry: Self) -> Result<(), NewzError> {
        diesel::insert_into(entry::table)
        .values(_entry)
        .execute(conn)?;

        Ok(())
    }
}

impl From<Entry> for GRPCEntry {
    fn from(_entry: Entry) -> GRPCEntry {
        let mut result = GRPCEntry::new();
        result.set_id(_entry.id);
        result.set_link(_entry.link);
        result.set_feed_id(_entry.feed_id);
        result.set_dt_retrieved(_entry.dt_retrieved.format(DATETIME_FMT).to_string());
        result.set_dt_published(_entry.dt_published.format(DATETIME_FMT).to_string());
        result.set_dt_updated(_entry.dt_updated.format(DATETIME_FMT).to_string());
        result.set_raw_feed_entry(_entry.raw_feed_entry);
        result.set_title(_entry.title);
        result.set_authors(_entry.authors);
        result.set_article_body_html(_entry.article_body_html);
        result.set_article_body_plaintext(_entry.article_body_plaintext);

        result
    }
}

impl TryFrom<GRPCEntry> for Entry {
    type Error = NewzError;

    fn try_from(mut _entry: GRPCEntry) -> Result<Entry, Self::Error> {
        let _entry = Entry {
            id: _entry.take_id(),
            link: _entry.take_link(),
            feed_id: _entry.take_feed_id(),
            dt_retrieved: NaiveDateTime::parse_from_str(&_entry.take_dt_retrieved(), DATETIME_FMT)?,
            dt_published: NaiveDateTime::parse_from_str(&_entry.take_dt_published(), DATETIME_FMT)?,
            dt_updated: NaiveDateTime::parse_from_str(&_entry.take_dt_updated(), DATETIME_FMT)?,
            raw_feed_entry: _entry.take_raw_feed_entry(),
            title: _entry.take_title(),
            authors: _entry.take_authors(),
            article_body_html: _entry.take_article_body_html(),
            article_body_plaintext: _entry.take_article_body_plaintext(),
        };

        Ok(_entry)
    }
}

impl From<Result<Entry, NewzError>> for MaybeGetEntryResponse {
    fn from(entry_result: Result<Entry, NewzError>) -> Self {
        let mut response = MaybeGetEntryResponse::new();

        match entry_result {
            Ok(actual_entry) => {
                let mut ger = GetEntryResponse::new();
                ger.set_entry(actual_entry.into());
                response.set_response(ger);
            },
            Err(err) => {
                let mut error = Error::new();
                error.set_error_message("Error fetching Entry".into());
                response.set_error(error);
            },
        }

        response
    }
}
