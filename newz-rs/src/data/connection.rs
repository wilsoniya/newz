use diesel::connection::Connection;
use diesel::sqlite::SqliteConnection;
use diesel::pg::PgConnection;

pub fn establish_connection() -> SqliteConnection {
    let database_url = "/home/wilsoniya/devel/newz/newz-rs/newz.db";
    SqliteConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub fn establish_pg_connection() -> PgConnection {
    let user = "newz";
    let password = "newz";
    let dbname = "newz";
    let host = "localhost";
    let port = "6432";

    let database_url = format!(
        "postgresql://{user}:{password}@{host}:{port}/{dbname}",
        user=user,
        password=password,
        host=host,
        port=port,
        dbname=dbname,
    );

    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", &database_url))
}

