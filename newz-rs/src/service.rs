use std::convert::TryInto;

use grpc;

use crate::data::connection::establish_pg_connection;
use crate::newz_grpc::newz_grpc::{Entries, EntriesServer};
use crate::newz_grpc::newz::{
    AddEntryRequest,
    AddEntryResonse,
//  Entry,
    Error,
    GetEntryRequest,
    GetEntryResponse,
    MaybeAddEntryResponse,
    MaybeGetEntryResponse,
};

use crate::errors::NewzError;
use crate::models::Entry;

pub struct EntriesImpl;

impl Entries for EntriesImpl {
    fn get_entry(
        &self,
        options: grpc::RequestOptions,
        request: GetEntryRequest,
    ) -> ::grpc::SingleResponse<MaybeGetEntryResponse> {
        info!("get_entry request received id={}", request.id);
        let conn = establish_pg_connection();
        let entry = Entry::get(&conn, request.id).into();
        grpc::SingleResponse::completed(entry)
    }

    fn add_entry(
        &self,
        options: grpc::RequestOptions,
        mut request: AddEntryRequest,
    ) -> ::grpc::SingleResponse<MaybeAddEntryResponse> {
        info!("add_entry request received...");
        let conn = establish_pg_connection();
        let mut response = MaybeAddEntryResponse::new();

        let maybe_inserted_entry = request.take_entry().try_into()
            .map_err(|err: NewzError| err.into())
            .map(|_entry: Entry| _entry.into())
            .and_then(|_entry| Entry::insert(&conn, _entry));

        match maybe_inserted_entry {
            Ok(_) => {
                response.set_response(AddEntryResonse::new())
            },
            Err(e) => {
                let mut error = Error::new();
                error.set_error_message(e.to_string());
                info!("{}", e);
                response.set_error(error);
            }
        }

        grpc::SingleResponse::completed(response)
    }
}

pub fn get_server() -> grpc::rt::ServerServiceDefinition {
    EntriesServer::new_service_def(EntriesImpl)
}
