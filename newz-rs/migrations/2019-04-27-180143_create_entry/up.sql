-- Your SQL goes here

CREATE TABLE IF NOT EXISTS entry (
    id varchar(512) NOT NULL PRIMARY KEY,
    link varchar(2048) NOT NULL,
    feed_id varchar(512) NOT NULL,
    dt_retrieved timestamp NOT NULL,
    dt_published timestamp NOT NULL,
    dt_updated timestamp NOT NULL,
    raw_feed_entry text NOT NULL,
    title varchar(512) NOT NULL,
    authors varchar(256) NOT NULL,
    article_body_html text NOT NULL,
    article_body_plaintext text NOT NULL
);
