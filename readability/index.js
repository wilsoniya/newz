#!/usr/bin/env node
const jsdom = require('jsdom')
const readability = require('readability-node')
const https = require('https')
const request = require('request')
const process = require('process')

const { JSDOM } = jsdom

const main = () => {
    url = process.argv[2]

    if (!url) {
        console.error('missing url parameter')
        process.exit(1)
    }

    request(url, (error, response, body) => {
        if (error) {
            console.error(error)
            process.exit(2)
        }

        const dom = new JSDOM(body);
        Node = dom.window.Node
        const window = dom.window

        const document = window.document
        const loc = document.location;
        const uri = {
              spec: loc.href,
              host: loc.host,
              prePath: loc.protocol + "//" + loc.host,
              scheme: loc.protocol.substr(0, loc.protocol.indexOf(":")),
              pathBase: loc.protocol + "//" + loc.host + loc.pathname.substr(0, loc.pathname.lastIndexOf("/") + 1)
        };
        const article = new readability.Readability(uri, document).parse()
        console.log(JSON.stringify(article))
    })
};

main();
