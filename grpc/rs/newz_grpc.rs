// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]


// interface

pub trait Entries {
    fn get_entry(&self, o: ::grpc::RequestOptions, p: super::newz::GetEntryRequest) -> ::grpc::SingleResponse<super::newz::MaybeGetEntryResponse>;

    fn add_entry(&self, o: ::grpc::RequestOptions, p: super::newz::AddEntryRequest) -> ::grpc::SingleResponse<super::newz::MaybeAddEntryResponse>;
}

// client

pub struct EntriesClient {
    grpc_client: ::std::sync::Arc<::grpc::Client>,
    method_GetEntry: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::newz::GetEntryRequest, super::newz::MaybeGetEntryResponse>>,
    method_AddEntry: ::std::sync::Arc<::grpc::rt::MethodDescriptor<super::newz::AddEntryRequest, super::newz::MaybeAddEntryResponse>>,
}

impl ::grpc::ClientStub for EntriesClient {
    fn with_client(grpc_client: ::std::sync::Arc<::grpc::Client>) -> Self {
        EntriesClient {
            grpc_client: grpc_client,
            method_GetEntry: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/newz.Entries/GetEntry".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
            method_AddEntry: ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                name: "/newz.Entries/AddEntry".to_string(),
                streaming: ::grpc::rt::GrpcStreaming::Unary,
                req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
            }),
        }
    }
}

impl Entries for EntriesClient {
    fn get_entry(&self, o: ::grpc::RequestOptions, p: super::newz::GetEntryRequest) -> ::grpc::SingleResponse<super::newz::MaybeGetEntryResponse> {
        self.grpc_client.call_unary(o, p, self.method_GetEntry.clone())
    }

    fn add_entry(&self, o: ::grpc::RequestOptions, p: super::newz::AddEntryRequest) -> ::grpc::SingleResponse<super::newz::MaybeAddEntryResponse> {
        self.grpc_client.call_unary(o, p, self.method_AddEntry.clone())
    }
}

// server

pub struct EntriesServer;


impl EntriesServer {
    pub fn new_service_def<H : Entries + 'static + Sync + Send + 'static>(handler: H) -> ::grpc::rt::ServerServiceDefinition {
        let handler_arc = ::std::sync::Arc::new(handler);
        ::grpc::rt::ServerServiceDefinition::new("/newz.Entries",
            vec![
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/newz.Entries/GetEntry".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.get_entry(o, p))
                    },
                ),
                ::grpc::rt::ServerMethod::new(
                    ::std::sync::Arc::new(::grpc::rt::MethodDescriptor {
                        name: "/newz.Entries/AddEntry".to_string(),
                        streaming: ::grpc::rt::GrpcStreaming::Unary,
                        req_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                        resp_marshaller: Box::new(::grpc::protobuf::MarshallerProtobuf),
                    }),
                    {
                        let handler_copy = handler_arc.clone();
                        ::grpc::rt::MethodHandlerUnary::new(move |o, p| handler_copy.add_entry(o, p))
                    },
                ),
            ],
        )
    }
}
