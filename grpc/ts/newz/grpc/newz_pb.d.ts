import * as jspb from "google-protobuf"

export class Error extends jspb.Message {
  getErrorMessage(): string;
  setErrorMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Error.AsObject;
  static toObject(includeInstance: boolean, msg: Error): Error.AsObject;
  static serializeBinaryToWriter(message: Error, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Error;
  static deserializeBinaryFromReader(message: Error, reader: jspb.BinaryReader): Error;
}

export namespace Error {
  export type AsObject = {
    errorMessage: string,
  }
}

export class Entry extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getLink(): string;
  setLink(value: string): void;

  getFeedId(): string;
  setFeedId(value: string): void;

  getDtRetrieved(): string;
  setDtRetrieved(value: string): void;

  getDtPublished(): string;
  setDtPublished(value: string): void;

  getDtUpdated(): string;
  setDtUpdated(value: string): void;

  getRawFeedEntry(): string;
  setRawFeedEntry(value: string): void;

  getTitle(): string;
  setTitle(value: string): void;

  getAuthors(): string;
  setAuthors(value: string): void;

  getArticleBodyHtml(): string;
  setArticleBodyHtml(value: string): void;

  getArticleBodyPlaintext(): string;
  setArticleBodyPlaintext(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Entry.AsObject;
  static toObject(includeInstance: boolean, msg: Entry): Entry.AsObject;
  static serializeBinaryToWriter(message: Entry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Entry;
  static deserializeBinaryFromReader(message: Entry, reader: jspb.BinaryReader): Entry;
}

export namespace Entry {
  export type AsObject = {
    id: string,
    link: string,
    feedId: string,
    dtRetrieved: string,
    dtPublished: string,
    dtUpdated: string,
    rawFeedEntry: string,
    title: string,
    authors: string,
    articleBodyHtml: string,
    articleBodyPlaintext: string,
  }
}

export class GetEntryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEntryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetEntryRequest): GetEntryRequest.AsObject;
  static serializeBinaryToWriter(message: GetEntryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEntryRequest;
  static deserializeBinaryFromReader(message: GetEntryRequest, reader: jspb.BinaryReader): GetEntryRequest;
}

export namespace GetEntryRequest {
  export type AsObject = {
    id: string,
  }
}

export class MaybeGetEntryResponse extends jspb.Message {
  getError(): Error | undefined;
  setError(value?: Error): void;
  hasError(): boolean;
  clearError(): void;
  hasError(): boolean;

  getResponse(): GetEntryResponse | undefined;
  setResponse(value?: GetEntryResponse): void;
  hasResponse(): boolean;
  clearResponse(): void;
  hasResponse(): boolean;

  getOptionCase(): MaybeGetEntryResponse.OptionCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MaybeGetEntryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: MaybeGetEntryResponse): MaybeGetEntryResponse.AsObject;
  static serializeBinaryToWriter(message: MaybeGetEntryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MaybeGetEntryResponse;
  static deserializeBinaryFromReader(message: MaybeGetEntryResponse, reader: jspb.BinaryReader): MaybeGetEntryResponse;
}

export namespace MaybeGetEntryResponse {
  export type AsObject = {
    error?: Error.AsObject,
    response?: GetEntryResponse.AsObject,
  }

  export enum OptionCase { 
    OPTION_NOT_SET = 0,
    ERROR = 10,
    RESPONSE = 20,
  }
}

export class GetEntryResponse extends jspb.Message {
  getEntry(): Entry | undefined;
  setEntry(value?: Entry): void;
  hasEntry(): boolean;
  clearEntry(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEntryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetEntryResponse): GetEntryResponse.AsObject;
  static serializeBinaryToWriter(message: GetEntryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEntryResponse;
  static deserializeBinaryFromReader(message: GetEntryResponse, reader: jspb.BinaryReader): GetEntryResponse;
}

export namespace GetEntryResponse {
  export type AsObject = {
    entry?: Entry.AsObject,
  }
}

export class AddEntryRequest extends jspb.Message {
  getEntry(): Entry | undefined;
  setEntry(value?: Entry): void;
  hasEntry(): boolean;
  clearEntry(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddEntryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddEntryRequest): AddEntryRequest.AsObject;
  static serializeBinaryToWriter(message: AddEntryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddEntryRequest;
  static deserializeBinaryFromReader(message: AddEntryRequest, reader: jspb.BinaryReader): AddEntryRequest;
}

export namespace AddEntryRequest {
  export type AsObject = {
    entry?: Entry.AsObject,
  }
}

export class MaybeAddEntryResponse extends jspb.Message {
  getError(): Error | undefined;
  setError(value?: Error): void;
  hasError(): boolean;
  clearError(): void;
  hasError(): boolean;

  getResponse(): AddEntryResonse | undefined;
  setResponse(value?: AddEntryResonse): void;
  hasResponse(): boolean;
  clearResponse(): void;
  hasResponse(): boolean;

  getOptionCase(): MaybeAddEntryResponse.OptionCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MaybeAddEntryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: MaybeAddEntryResponse): MaybeAddEntryResponse.AsObject;
  static serializeBinaryToWriter(message: MaybeAddEntryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MaybeAddEntryResponse;
  static deserializeBinaryFromReader(message: MaybeAddEntryResponse, reader: jspb.BinaryReader): MaybeAddEntryResponse;
}

export namespace MaybeAddEntryResponse {
  export type AsObject = {
    error?: Error.AsObject,
    response?: AddEntryResonse.AsObject,
  }

  export enum OptionCase { 
    OPTION_NOT_SET = 0,
    ERROR = 10,
    RESPONSE = 20,
  }
}

export class AddEntryResonse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddEntryResonse.AsObject;
  static toObject(includeInstance: boolean, msg: AddEntryResonse): AddEntryResonse.AsObject;
  static serializeBinaryToWriter(message: AddEntryResonse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddEntryResonse;
  static deserializeBinaryFromReader(message: AddEntryResonse, reader: jspb.BinaryReader): AddEntryResonse;
}

export namespace AddEntryResonse {
  export type AsObject = {
  }
}

