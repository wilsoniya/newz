/**
 * @fileoverview gRPC-Web generated client stub for newz
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.newz = require('./newz_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.newz.EntriesClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.newz.EntriesPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.newz.GetEntryRequest,
 *   !proto.newz.MaybeGetEntryResponse>}
 */
const methodInfo_Entries_GetEntry = new grpc.web.AbstractClientBase.MethodInfo(
  proto.newz.MaybeGetEntryResponse,
  /** @param {!proto.newz.GetEntryRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.newz.MaybeGetEntryResponse.deserializeBinary
);


/**
 * @param {!proto.newz.GetEntryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.newz.MaybeGetEntryResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.newz.MaybeGetEntryResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.newz.EntriesClient.prototype.getEntry =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/newz.Entries/GetEntry',
      request,
      metadata || {},
      methodInfo_Entries_GetEntry,
      callback);
};


/**
 * @param {!proto.newz.GetEntryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.newz.MaybeGetEntryResponse>}
 *     A native promise that resolves to the response
 */
proto.newz.EntriesPromiseClient.prototype.getEntry =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/newz.Entries/GetEntry',
      request,
      metadata || {},
      methodInfo_Entries_GetEntry);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.newz.AddEntryRequest,
 *   !proto.newz.MaybeAddEntryResponse>}
 */
const methodInfo_Entries_AddEntry = new grpc.web.AbstractClientBase.MethodInfo(
  proto.newz.MaybeAddEntryResponse,
  /** @param {!proto.newz.AddEntryRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.newz.MaybeAddEntryResponse.deserializeBinary
);


/**
 * @param {!proto.newz.AddEntryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.newz.MaybeAddEntryResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.newz.MaybeAddEntryResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.newz.EntriesClient.prototype.addEntry =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/newz.Entries/AddEntry',
      request,
      metadata || {},
      methodInfo_Entries_AddEntry,
      callback);
};


/**
 * @param {!proto.newz.AddEntryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.newz.MaybeAddEntryResponse>}
 *     A native promise that resolves to the response
 */
proto.newz.EntriesPromiseClient.prototype.addEntry =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/newz.Entries/AddEntry',
      request,
      metadata || {},
      methodInfo_Entries_AddEntry);
};


module.exports = proto.newz;

