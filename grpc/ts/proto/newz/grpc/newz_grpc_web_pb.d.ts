import * as grpcWeb from 'grpc-web';

import {
  AddEntryRequest,
  GetEntryRequest,
  MaybeAddEntryResponse,
  MaybeGetEntryResponse} from './newz_pb';

export class EntriesClient {
  constructor (hostname: string,
               credentials: null | { [index: string]: string; },
               options: null | { [index: string]: string; });

  getEntry(
    request: GetEntryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: MaybeGetEntryResponse) => void
  ): grpcWeb.ClientReadableStream<MaybeGetEntryResponse>;

  addEntry(
    request: AddEntryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: MaybeAddEntryResponse) => void
  ): grpcWeb.ClientReadableStream<MaybeAddEntryResponse>;

}

export class EntriesPromiseClient {
  constructor (hostname: string,
               credentials: null | { [index: string]: string; },
               options: null | { [index: string]: string; });

  getEntry(
    request: GetEntryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<MaybeGetEntryResponse>;

  addEntry(
    request: AddEntryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<MaybeAddEntryResponse>;

}

