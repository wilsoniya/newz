package wilsoniya.extractor

import java.net.URI
import java.io.StringWriter
import java.io.StringBufferInputStream

import de.l3s.boilerpipe.extractors.ArticleExtractor
import org.apache.tika.Tika
import org.apache.tika.detect
import org.apache.tika.parser
import org.apache.tika.metadata
import org.apache.tika.parser.html.BoilerpipeContentHandler
import com.softwaremill.sttp._


object Extractor {
  //def fromUrl(url: URI): String = {
  //  ArticleExtractor.INSTANCE.getText(url)
  //}

  def fromUrl(url: URI): String = {
    //ArticleExtractor.INSTANCE.getText(url)
    val detector = new detect.DefaultDetector()
    val _parser = new parser.AutoDetectParser()
    val _metadata = new metadata.Metadata()
    val input = new StringBufferInputStream(Extractor.fetch(url))
    val writer = new StringWriter()
    val handler = new BoilerpipeContentHandler(writer)
    val context = new parser.ParseContext()

    _parser.parse(input, handler, _metadata, context)
    writer.toString
  }

  private def fetch(url: URI): String = {
    val request = sttp.get(Uri(url))
    implicit val backend = HttpURLConnectionBackend()
    val response = request.send()
    response.unsafeBody
  }
}

