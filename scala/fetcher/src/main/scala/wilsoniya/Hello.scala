package wilsoniya

import java.net.URI

import wilsoniya.extractor.Extractor

object Hello  {
  def main(args: Array[String]): Unit = {
    val url = new URI("https://www.theguardian.com/us-news/2018/feb/25/democrats-russia-nunes-memo-donald-trump-criticism")
    //val url = new URI("https://newyorktimes.com")
    val article = Extractor.fromUrl(url)
    article.split("\n") zip (1 to 1000) foreach { case (text, idx) => println(s"$idx: $text") }
  }
}
