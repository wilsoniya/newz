import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.4",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "fetcher",

    libraryDependencies ++= Seq(
        scalaTest % Test,

        // https://mvnrepository.com/artifact/de.l3s.boilerpipe/boilerpipe
        "de.l3s.boilerpipe" % "boilerpipe" % "1.1.0",
        "com.softwaremill.sttp" %% "core" % "1.1.6",
        // https://mvnrepository.com/artifact/xerces/xercesImpl
        "xerces" % "xercesImpl" % "2.9.1",
        // https://mvnrepository.com/artifact/net.sourceforge.nekohtml/nekohtml
        "net.sourceforge.nekohtml" % "nekohtml" % "1.9.22",
        // https://mvnrepository.com/artifact/org.apache.tika/tika-core
        "org.apache.tika" % "tika-core" % "1.17",
        // https://mvnrepository.com/artifact/org.apache.tika/tika-parsers
        "org.apache.tika" % "tika-parsers" % "1.17",
    )
  )
